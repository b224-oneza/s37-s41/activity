const Course = require("../models/Course");


// Controller Functions:

// Creating a new course 
// module.exports.addCourse = (reqBody) => {

//     let newCourse = new Course({
//         name: reqBody.name,
//         description: reqBody.description,
//         price: reqBody.price
//     });

//     return newCourse.save().then((course, error) => {
//         if(error) {
//             return false //"Course creation failed"
//         } else {
//             return true
//         }
//     })
// };

// ACTIVITY
module.exports.addCourse = (reqBody, userData) => {
    if (userData.isAdmin = true) {
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });

        return newCourse.save().then((course, error) => {
            if(error) {
                return false; 
            } else {
                return true;
            }
        });
    } else {
        return false;
    }
};

// Retrieving All Courses
module.exports.getAllCourses = (data) => {

    if(data.isAdmin) {
        return Course.find({}).then(result => {
            return result
        })
    } else {
        return false // "You are not an Admin"
    }
};

// Retrieve All Active Courses
module.exports.getAllActive = () => {

    return Course.find({isActive: true}).then(result => {
        return result
    })
};


// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {

    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
};


// Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {

    let updateCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((updateCourse, error) => {

        console.log(updateCourse)
        if(error) {
            return false
        } else {
            return true
        }
    })
};


// ACTIVITY
module.exports.archiveCourse = (reqParams, reqBody) => {
    
    let archiveCourse = {
        isActive: reqBody.isActive
    }

    return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((archiveCourse, error) => {
        
        console.log(archiveCourse)
        if(error) {
            return false
        } else {
            return true
        }
    })
}