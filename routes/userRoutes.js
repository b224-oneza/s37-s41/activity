const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require("../auth");


//Routes


// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "app.js" file
// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
// Route for checking email
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user log in
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});


// S38 Activity Solution
// Route for getting user's detail
router.post("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    console.log(req.headers.authorization);

    userController.getProfile({id: userData.id}).then(resultFromController => res.send (resultFromController))
});


// Solution 2
// router.post("/details", (req, res) => {
//     userController.getProfile(req.body).then(resultFromController => res.send (resultFromController))
// });



// ACTIVITY s41
// Refactor
router.post("/enroll", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    
    let data = {
        userId: userData.id,
        courseId: req.body.courseId
    }
    
    if(userData.isAdmin === false) {
        userController.enroll(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send({auth: "Admin is not allowed"})
    }
    
});



// Route for enrolling an authenticated user
// router.post("/enroll", (req, res) => {
    
//     let data = {
//         userId: req.body.userId,
//         courseId: req.body.courseId
//     }

//     userController.enroll(data).then(resultFromController => res.send(resultFromController))
// });






module.exports = router;