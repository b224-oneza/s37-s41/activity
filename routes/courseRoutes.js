const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");


// Routes
// Route for creating a course
// router.post("/addCourse", (req, res) => {
//     courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// });


// Activity
router.post("/addCourse", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData);
    console.log(req.headers.authorization);

    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
});



// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all active courses
router.get("/", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

router.get("/:courseId", (req, res) => {
    console.log(req.params.courseId)

    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});


// ACTIVITY s40
// Route for archiving a course
router.put("/archive/:courseId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
    } else {
        res.send({auth: "failed"})
    }

});










// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
    } else {
        res.send({auth: "failed"})
    }
    
});









module.exports = router;